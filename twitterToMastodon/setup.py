from setupUtils import Setup
import sys, os


def main():
    config = {}
    # Create needed Folders
    try:
        os.makedirs('images')
        os.makedirs('log')
    except FileExistsError:
        print('Folders already exist')

    setup = Setup(config)
    print('Welcome to the setup for the Twitter to Mastodon mirror bot.')
    setup.set_app_name()
    print('App name set')

    setup.register_mastodon_app()
    print('Bot registered')

    setup.create_mastodon_login()
    print('Bot Login created')

    setup.setup_twitter()
    setup.save_config()
    print("Setup completed please run bot.py (Example: python3 bot.py @cnn)")
    sys.exit()


if __name__ == '__main__':
    main()
