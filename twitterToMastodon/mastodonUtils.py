from mastodon import Mastodon, MastodonError
import time, logging

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

formatter = logging.Formatter('%(asctime)s:%(name)s:%(levelname)s:%(message)s')
file_handler = logging.FileHandler('log/mastodonUtils.log')
file_handler.setFormatter(formatter)

logger.addHandler(file_handler)


class MastodonUtils():

    def __init__(self, config):
        self.config = config
        self.max_retries = 5

        self.mastodon = Mastodon(access_token=self.config['app_name'] + '_usercred.secret',
                                 api_base_url=self.config['mastodon_server'])

    def create_text_toot(self, text, *args):
        try:
            created = self.mastodon.status_post(text, None, None, False, 'public', None, None,
                                           None, 'text/html')
            logger.info('created toot: ' + str(created['id']))
        except MastodonError as e:
            logger.error(e)
            time.sleep(5)
            if not args:
                args = [0]
            for arg in args:
                if arg < self.max_retries:
                    self.create_text_toot(tweet, arg + 1)
                elif arg == self.max_retries:
                    return

    def create_media_toot(self, text, image_list, *args):
        # creating media toots for all images
        media_ids = []
        for image in image_list:
            media_ids.append(self.mastodon.media_post('images/' + image))

        # create toot
        try:
            created = self.mastodon.status_post(text, None, media_ids, False, 'public', None, None,
                                                None, 'text/html')
            logger.info('created toot: ' + str(created['id']))
        except MastodonError as e:
            logger.error(e)
            time.sleep(5)
            if not args:
                args = [0]
            for arg in args:
                if arg < self.max_retries:
                    self.create_media_toot(tweet, image_list, arg + 1)
                elif arg == self.max_retries:
                    return

