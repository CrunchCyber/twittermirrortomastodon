from mastodon import Mastodon, MastodonError
from twython import Twython
import json, os


class Setup:

    def __init__(self, config):
        self.config = config
        self.cleanup()

    @staticmethod
    def cleanup():
        filelist = [f for f in os.listdir('.') if f.endswith('.secret')]
        for f in filelist:
            os.remove(f)

    def set_app_name(self):
        self.config['app_name'] = input('Enter bot name (example: cnnBot): ')
        print('Name set to ' + self.config['app_name'])
        return True

    def register_mastodon_app(self):
        self.config['mastodon_server'] = input('Enter Mastodon/Pleroma server ')

        try:
            Mastodon.create_app(self.config['app_name'],
                                         api_base_url=self.config['mastodon_server'],
                                         to_file=self.config['app_name'] + '_clientcred.secret')
        except KeyError:
            print('Server not found, check if you spelled the url correctly')
            self.register_mastodon_app()


    def create_mastodon_login(self):
        email = input('Enter EMail: ')
        password = input('Enter Password: ')

        mastodon = Mastodon(client_id=self.config['app_name'] + '_clientcred.secret',
                            api_base_url=self.config['mastodon_server'])
        try:
            mastodon.log_in(email, password, to_file=self.config['app_name'] + '_usercred.secret' )
        except:
            print('Invalid credentials')
            self.create_mastodon_login()



    def setup_twitter(self):
        print("Please enter your Twitter API credentials, if you need to generate some check "
              "https://https://developer.twitter.com/")
        self.config['twitter_auth'] = {}
        self.config['twitter_auth']['app_key'] = input('Input Twitter app key: ')
        self.config['twitter_auth']['app_secret'] = input('Input Twitter app secret: ')
        self.config['twitter_auth']['oauth_token'] = input('Input Twitter Oauth token: ')
        self.config['twitter_auth']['oauth_secret'] = input('Input Twitter Oauth secret: ')

        twitter = Twython(self.config['twitter_auth']['app_key'],
                               self.config['twitter_auth']['app_secret'],
                               self.config['twitter_auth']['oauth_token'],
                               self.config['twitter_auth']['oauth_secret'])
        try:
            print("Authenticating Twitter credentials")
            twitter.verify_credentials()
        except:
            print('Invalid credentials')
            self.setup_twitter()

    def save_config(self):
        with open('config.json', 'w+') as fp:
            json.dump(self.config, fp)
