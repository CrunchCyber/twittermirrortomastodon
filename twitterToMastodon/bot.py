#!~/twitterToMastodon/env/bin/python
import json
import logging
import sys
from twitterUtils import TwitterUtils
from mastodonUtils import MastodonUtils
from tweepy import OAuthHandler
from tweepy import Stream
from tweepy.streaming import StreamListener

'''
Logging setup
'''
logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

formatter = logging.Formatter('%(asctime)s:%(name)s:%(levelname)s:%(message)s')
file_handler = logging.FileHandler('log/bot.log')
file_handler.setFormatter(formatter)

logger.addHandler(file_handler)


'''
Config
'''
with open('config.json') as json_file:
    config = json.load(json_file)

'''
Custom StreamListener
'''
class BotStreamListener(StreamListener):
    def __init__(self, config):
        self.util = TwitterUtils(config)
        self.mastodon = MastodonUtils(config)
        self.config = config


    def on_data(self, data):
        try:
            tweet = json.loads(data)
            tweet_text = self.util.parse_tweet(tweet)
            # check if tweet is done by filter
            if tweet['user']['id_str'] is config['user_id']:
                if 'media' in tweet['entities']:
                    image_list = self.util.retrieve_all_images(tweet)
                    if image_list.startswith('<a href='):
                        tweet_text = tweet_text + ' ' + image_list
                        self.mastodon.create_text_toot(tweet_text)
                        logger.info('Created new toot with linked video')
                    else:
                        self.mastodon.create_media_toot(tweet_text, image_list)
                        logger.info('Created new media toot')
                    return True
                self.mastodon.create_text_toot(tweet_text)
                logger.info('Created new toot')
                return True
        except BaseException as e:
            logger.error(e)
            return True


    def on_error(self, status):
        logger.error(status)


# main loop
def main():
    # Twitter auth
    config['user_str'] = sys.argv[1]
    logger.info('Mirroring tweets for user ' + sys.argv[1])
    auth = OAuthHandler(config['twitter_auth']['app_key'],
                        config['twitter_auth']['app_secret'])
    auth.set_access_token(config['twitter_auth']['oauth_token'],
                          config['twitter_auth']['oauth_secret'])

    #return user id from screen name
    TwitterUtils.get_user_id(auth, config)

    listener = BotStreamListener(config)
    stream = Stream(auth, listener)
    stream.filter(follow=[config['user_id']])

if __name__ == '__main__':
    main()
