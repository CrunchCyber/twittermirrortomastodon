from urllib.request import urlretrieve
from urllib.error import URLError
import time, re, logging
import json
import os
import tweepy


logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

formatter = logging.Formatter('%(asctime)s:%(name)s:%(levelname)s:%(message)s')
file_handler = logging.FileHandler('log/twitterUtils.log')
file_handler.setFormatter(formatter)

logger.addHandler(file_handler)


class TwitterUtils:
    def __init__(self, config):
        self.config = config


    @staticmethod
    def get_user_id(auth, config):
        api = tweepy.API(auth)
        user = api.get_user(screen_name=config['user_str'][1:])
        config['user_id'] = user._json['id_str']
        logger.info('Retrieved user id {} for handle {}'.format(config['user_id'],
                                                                config['user_str']))


    def parse_tweet(self, tweet):
        if 'retweeted_status' in tweet:
            if 'extended_tweet' in tweet['retweeted_status']:
                tweet = self.append_rt(tweet)
                tweet_text = tweet['retweeted_status']['extended_tweet']['full_text']
                tweet_text = self.replace_handle(tweet_text)
                tweet_text = self.append_url(tweet_text, tweet)
            else:
                tweet = self.append_rt(tweet)
                tweet_text = tweet['retweeted_status']['text']
                tweet_text = self.replace_handle(tweet_text)
                tweet_text = self.append_url(tweet_text, tweet)
        elif 'extended_tweet' in tweet:
            tweet_text = tweet['extended_tweet']['full_text']
            tweet_text = self.append_url(tweet_text, tweet)
            tweet_text = self.replace_handle(tweet_text)
        else:
            tweet_text = self.append_url(tweet['text'], tweet)
            tweet_text = self.replace_handle(tweet_text)
        return tweet_text

    @staticmethod
    def append_rt(tweet):
        if 'extended_tweet' in tweet['retweeted_status']:
            tweet['retweeted_status']['extended_tweet']['full_text'] = 'Retweeted: <br>' + tweet['retweeted_status']['extended_tweet']['full_text']
        else:
            tweet['retweeted_status']['text'] = 'Retweeted: <br>' + tweet['retweeted_status']['text']
        return tweet

    @staticmethod
    def append_url(text, tweet):
        text += '<br><a href="http://twitter.com/user/status/' + str(tweet['id']) + '">Original Tweet</a>'
        return text

    def replace_handle(self, text):
        handles = self.find_all_handles(text)
        for handle in handles:
            url = '<a href="https://twitter.com/' + handle[1:] + '">' + handle + '</a>'
            text = text.replace(handle, url)
        return text

    @staticmethod
    def find_all_handles(tweet):
        output = []
        for m in re.finditer('@', tweet):
            handle_end = tweet.find(' ', m.start())
            if handle_end == -1:
                handle_end = len(tweet)
            output.append(tweet[m.start():handle_end])
        return output

    def retrieve_all_images(self, tweet):
        image_list = []
        for media in tweet['entities']['media']:
            if media['type'] == 'video':
                dl_string= 'youtube-dl  https://twitter.com/' + tweet['screen_name'] + '/status/' + tweet['id_str'] + '--i -o /var/www/html/' + tweet['screen_name']
                os.system(dl_string)
                return '<a href="yaniv.k1ll.site/' + tweet['id_str'] + '.mp4">Link to attached Video</a>'
            else:
                media_url = media['media_url_https']
                file_name = re.split('/', media_url)[-1]
                try:
                    urlretrieve(media_url, 'images/' + file_name)
                    image_list.append(file_name)
                except URLError as e:
                    logger.error(e)

        return image_list

