# twitterToMastodon

A bot to mirror tweets to a mastodon/pleroma instance.

## Getting Started

Read Installing to get started.

### Prerequisites

Python3

### Installing

* pip install -r requirements.txt
* run setup.py and follow the instructions
* run bot.py <twitterHandle> (@example)

## Authors

* **me** - *All the work*


## License

BSD-3-Clause-No-Nuclear-License

## Acknowledgments

* There may be bugs


